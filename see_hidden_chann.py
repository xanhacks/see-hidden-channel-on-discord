#!/usr/bin/env python3
import requests, argparse

parser = argparse.ArgumentParser(description="Discover hidden channels in Discord servers")
parser.add_argument("-id", "--server-id", help="The Discord server ID")
parser.add_argument("-t", "--authorization-token", help="Your Discord authorization token")
args = parser.parse_args()

def main(serv_id, token):
    url = f"https://discordapp.com/api/v6/guilds/{serv_id}/channels"

    headers = {
        "Authorization": token,
    }

    channels = requests.get(url, headers=headers).json()

    channel_type = {
        0: "Text channel",
        1: "Direct message",
        2: "Voice channel",
        3: "Group DM",
        4: "Server Category",
        5: "News channel",
        6: "Store channel",
    }

    for channel in channels:
        try:
            print(f"Name: {channel['name']}")
            print(f"URL: https://discordapp.com/channels/{serv_id}/{channel['id']}")
            print(f"Type: {channel_type[channel['type']]}")

            if "topic" in channel.keys() and channel["topic"]:
                print(f"Topic: {channel['topic']}")

            print(f"Position: {channel['position']}")

            if channel["nsfw"]:
                print(f"NSFW: ON")
            print("=" * 30)
        except:
            print("\n-----> [-] Invalid ID or token\n")
            parser.print_help()
            break


if __name__ == "__main__":
    if args.server_id and args.authorization_token:
        main(args.server_id, args.authorization_token)
    else:
        parser.print_help()
